package main

import (
	codecDeposit "bibit/codec"
	"bibit/dto"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/lovoo/goka"
	"github.com/lovoo/goka/codec"
	"log"
	"net/http"
	"time"
)

var (
	brokers                         = []string{"127.0.0.1:29092"}
	topic               goka.Stream = "deposit"
	group               goka.Group  = "balance"
	aboveThresholdGroup goka.Group  = "aboveThreshold"

	tmc *goka.TopicManagerConfig
)

func init() {
	tmc = goka.NewTopicManagerConfig()
	tmc.Table.Replication = 1
	tmc.Stream.Replication = 1
}

func main() {
	tm, err := goka.NewTopicManager(brokers, goka.DefaultConfig(), tmc)
	if err != nil {
		log.Fatalf("Error creating topic manager: %v", err)
	}
	defer tm.Close()
	err = tm.EnsureStreamExists(string(topic), 8)
	if err != nil {
		log.Printf("Error creating kafka topic %s: %v", topic, err)
	}

	initialized := make(chan struct{})

	go runProcessor(initialized)
	runView(initialized)
}

func runProcessor(initialized chan struct{}) {
	g := goka.DefineGroup(group,
		goka.Input(topic, new(codec.String), process),
		goka.Persist(new(codecDeposit.DepositCodec)),
	)
	p, err := goka.NewProcessor(brokers,
		g,
		goka.WithTopicManagerBuilder(goka.TopicManagerBuilderWithTopicManagerConfig(tmc)),
		goka.WithConsumerGroupBuilder(goka.DefaultConsumerGroupBuilder),
	)
	if err != nil {
		panic(err)
	}

	aboveThreshold := goka.DefineGroup(aboveThresholdGroup,
		goka.Input(topic, new(codec.String), AboveThresholdProcess),
		goka.Persist(new(codecDeposit.DepositCodec)),
	)
	processorAboveThresholdGroup, err := goka.NewProcessor(brokers,
		aboveThreshold,
		goka.WithTopicManagerBuilder(goka.TopicManagerBuilderWithTopicManagerConfig(tmc)),
		goka.WithConsumerGroupBuilder(goka.DefaultConsumerGroupBuilder),
	)

	if err != nil {
		panic(err)
	}

	close(initialized)

	go processorAboveThresholdGroup.Run(context.Background())
	go p.Run(context.Background())
}

func process(ctx goka.Context, msg interface{}) {
	var u, o *dto.Deposit

	if val := ctx.Value(); val != nil {
		o = val.(*dto.Deposit)
		err := json.Unmarshal([]byte(fmt.Sprintf("%v", msg)), &u)
		if err != nil {
			return
		}

		if o.WalledId == u.WalledId {
			o.Amount += u.Amount
		}

	} else {
		o = new(dto.Deposit)

		err := json.Unmarshal([]byte(fmt.Sprintf("%v", msg)), &o)
		if err != nil {
			return
		}
	}

	ctx.SetValue(o)
}

func AboveThresholdProcess(ctx goka.Context, msg interface{}) {
	var u, o *dto.Deposit

	ticker := time.NewTicker(time.Minute * 2)

	select {
	case <-ticker.C:
		ctx.Delete()
	default:
		if val := ctx.Value(); val != nil {
			o = val.(*dto.Deposit)
			err := json.Unmarshal([]byte(fmt.Sprintf("%v", msg)), &u)
			if err != nil {
				return
			}

			if o.WalledId == u.WalledId {
				o.Amount += u.Amount
			}

		} else {
			o = new(dto.Deposit)

			err := json.Unmarshal([]byte(fmt.Sprintf("%v", msg)), &o)
			if err != nil {
				return
			}
		}

		ctx.SetValue(o)
	}

}

func runView(initialized chan struct{}) {
	<-initialized

	view, err := goka.NewView(brokers,
		goka.GroupTable(group),
		new(codecDeposit.DepositCodec),
	)
	if err != nil {
		panic(err)
	}

	aboveThresholdView, err := goka.NewView(brokers,
		goka.GroupTable(aboveThresholdGroup),
		new(codecDeposit.DepositCodec),
	)
	if err != nil {
		panic(err)
	}

	go view.Run(context.Background())
	go aboveThresholdView.Run(context.Background())

	root := mux.NewRouter()
	root.HandleFunc("/deposit", func(w http.ResponseWriter, r *http.Request) {
		var deposit *dto.Deposit
		json.NewDecoder(r.Body).Decode(&deposit)

		emitter, err := goka.NewEmitter(brokers, topic,
			new(codec.String))
		if err != nil {
			panic(err)
		}

		defer emitter.Finish()
		dJSON, err := json.Marshal(deposit)
		if err != nil {
			log.Fatalf("error marshal: %v", err)
		}

		emitter.EmitSync(deposit.WalledId, string(dJSON))
		w.Header().Add("Content-Type", "application/json")
		w.Write(dJSON)
	}).Methods("POST")

	root.HandleFunc("/{id}/detail", func(w http.ResponseWriter, r *http.Request) {
		value, _ := view.Get(mux.Vars(r)["id"])
		valueAboveThreshold, _ := aboveThresholdView.Get(mux.Vars(r)["id"])
		aboveThreshold := false
		balance := value.(*dto.Deposit)
		if valueAboveThreshold != nil {
			aboveDeposit := valueAboveThreshold.(*dto.Deposit)
			if aboveDeposit.Amount > 10000 {
				aboveThreshold = true
			}
		}

		resp := dto.DepositResponse{
			WalledId:       balance.WalledId,
			Amount:         balance.Amount,
			AboveThreshold: aboveThreshold,
		}

		data, _ := json.Marshal(resp)
		w.Header().Add("Content-Type", "application/json")
		w.Write(data)
	}).Methods("GET")
	fmt.Println("View opened at http://localhost:8080/")
	http.ListenAndServe(":8080", root)
}
