package dto

type Deposit struct {
	WalledId string  `json:"wallet_id"`
	Amount   float64 `json:"amount"`
}
