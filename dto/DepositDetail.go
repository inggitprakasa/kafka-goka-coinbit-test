package dto

type DepositDetail struct {
	Amount         float64 `json:"amount"`
	AboveThreshold bool    `json:"above_threshold:"`
}
