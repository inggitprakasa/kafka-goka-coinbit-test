package codec

import (
	"bibit/dto"
	"encoding/json"
	"fmt"
)

type DepositCodec struct{}

func (jc *DepositCodec) Encode(value interface{}) ([]byte, error) {
	if _, isDeposit := value.(*dto.Deposit); !isDeposit {
		return nil, fmt.Errorf("Codec requires value *Deposit, got %T", value)
	}
	return json.Marshal(value)
}

func (jc *DepositCodec) Decode(data []byte) (interface{}, error) {
	var (
		c   dto.Deposit
		err error
	)
	err = json.Unmarshal(data, &c)
	if err != nil {
		return nil, fmt.Errorf("Error unmarshaling Deposit: %v", err)
	}
	return &c, nil
}
